<?php

use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Model\MimeType;

define('QB_SETTINGS', 'settings');
define('QB_SESSION', 'session');
define('QB_LOGGER', 'logger');
define('QB_VIEW', 'view');
define('QB_CACHE', 'cache');
define('QB_AUTH_QBANK', 'auth');
define('QB_AUTH', 'auth');
define('QB_ROUTER', 'router');
define('QB_QBANKAPI', 'qbankapi');
define('QB_SESSION_KEY', 'sessionId');
define('QB_FILTER_CONTROLLER', 'filtercontroller');
define('QB_SELECTION_CONTROLLER', 'selectioncontroller');
define('QB_MAILCONTROLLER', 'mailcontroller');
define('QB_FRONTEND', 'frontend');
define('QB_CATEGORIES', 'qbcategories');
define('QB_IP_AUTO_LOGIN', 'ipautologin');
define('QB_SOURCE_ID', 'source_id');

return [
	QB_SETTINGS => [
		// Slim Settings
		'determineRouteBeforeAppMiddleware' => true,

		'displayErrorDetails' => false,
		'debug' => false,
		'whoops.editor' => 'phpstorm',

		// Session settings
		QB_SESSION => [
			'name' => 'qbankfrontendsession',
			'autorefresh' => true,
			'lifetime' => '1 hour'
		],

		// Monolog settings
		QB_LOGGER => [
			'name' => 'app',
			'path' => __DIR__ . '/../logs/app.log',
		],

		// PlatesPhp settings
		QB_VIEW => [
			'directory' => __DIR__ . '/../src/templates',
			'assetPath' => __DIR__ . '/../public/',
			'fileExtension' => 'php',
			'timestampInFilename' => false,
		],

		QB_CACHE => [
			'directory' => __DIR__ . '/../cache/'
		],

		QB_AUTH => [
			'loginRoute' => 'login'
		],

		// QBankApi settings (these settings should be defined in settings.<hostname>.php
		QB_QBANKAPI => [
			'url' => '',
			'client_id' => '',
			'username' => '',
			'password' => '',
			'localDeployPath' => '/storage/deploy/client.qbank.se/',
			'cache' => [
				'type' => CachePolicy::EVERYTHING,
				'ttl' => 3600 * 4 // 4 hours
			],
			'sourceId' => 0,
			'categoryIds' => [1],
			'deploymentSiteIds' => [1]
		],

		QB_FRONTEND => [
			'thumbnails' => [
				'small' => 20,
				'large' => 21,
				'preview' => 19,
				'hoverthumb' => 18
			],
			'downloadTemplates' => [
				MimeType::CLASSIFICATION_IMAGE => [
					'Original' => null,
					'High-res' => 1,
					'Low-res' => 2
				],
				MimeType::CLASSIFICATION_VIDEO => [
					'Original' => null,
					'Web' => 1
				],
				MimeType::CLASSIFICATION_DOCUMENT => [
					'Original' => null
				],
				MimeType::CLASSIFICATION_ARCHIVE => [
					'Original' => null
				],
				MimeType::CLASSIFICATION_FONT => [
					'Original' => null
				],
				MimeType::CLASSIFICATION_AUDIO => [
					'Original' => null
				]
			],
			'selectionDownloadTemplates' => [
				'Original' => null,
				'High-res' => 1,
				'Low-res' => 2
			]
		],
		QB_IP_AUTO_LOGIN => [
			'allowedIps' => []
		],
	]
];
