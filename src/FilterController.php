<?php

namespace QBNK\FrontendComponents;

use Dflydev\DotAccessData\Data;
use QBNK\FrontendComponents\Filter\BaseOptions;
use QBNK\FrontendComponents\Filter\CategoryOptions;
use QBNK\FrontendComponents\Filter\FolderOptions;
use QBNK\FrontendComponents\Filter\FreeTextOptions;
use QBNK\FrontendComponents\Filter\Item;
use QBNK\FrontendComponents\Filter\PropertyOptions;
use QBNK\FrontendComponents\Filter\Section;
use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Model\FilterItem;
use QBNK\QBank\API\Model\PropertyCriteria;
use QBNK\QBank\API\Model\Search;
use QBNK\QBank\API\QBankApi;

class FilterController
{
	public const FILTER_INTERSECT = 'intersect';
	public const FILTER_UNION = 'union';
	public const FREETEXT_OR = 'or';
	public const FREETEXT_AND = 'and';
	public const TYPE_CATEGORY = 'category';
	public const TYPE_PROPERTY = 'property';
	public const TYPE_FOLDER = 'folder';
	public const TYPE_FREETEXT = 'freetext';

	/** @var QBankApi */
	protected $qbankApi;

	/** @var CachePolicy */
	protected $cachePolicy;

	/** @var int[] */
	protected $deploymentSiteIds;

	/** @var int[] */
	protected $categoryIds;

	/** @var int[] */
	protected $freeTextMediaIds;

	/** @var string[] */
	protected $selectedFilters;

	/** @var Filter\Section[] */
	protected $filters;

	/** @var bool[] MediaId is the key. */
	public static $allMediaIds = [];

	/** @var Filter\Item[] */
	public static $activeItems = [];

	/** @var array */
	public static $separators;

	/** @var bool */
	public $ignoreMediaIds = false;

	/**
	 * FilterController constructor.
	 *
	 * @param QBankApi $qbankApi
	 * @param int[] $deploymentSiteIds
	 * @param string $pathinfo
	 * @param array $separators
	 */
	public function __construct(QBankApi $qbankApi, array $deploymentSiteIds, $pathinfo, $separators = [])
    {
		$defaultSeparators = [
			'propertySeparator' => '/',
			'hierarchicalSeparator' => '--',
			'hierarchicalLeafSeparator' => ','
		];
		self::$separators = $separators = array_merge($defaultSeparators, $separators);

		$this->qbankApi = $qbankApi;
		$this->deploymentSiteIds = filter_var_array($deploymentSiteIds, FILTER_VALIDATE_INT);
		$this->freeTextMediaIds = [];

		$this->selectedFilters = new Data();
		$path = array_filter(explode($separators['propertySeparator'], str_replace('&', '%26', $pathinfo)));
		foreach ($path as $part) {
			$part = explode($separators['hierarchicalSeparator'], $part);
			$leaf = (count($part) > 1) ? explode($separators['hierarchicalLeafSeparator'], array_pop($part)) : $part;
			$leaf = array_combine($leaf, array_fill(0, count($leaf), null));

			$key = implode('.', $part);
			if ($this->selectedFilters->has($key)) {
				$current = $this->selectedFilters->get($key);
				if (is_array($current)) {
					$this->selectedFilters->set($key, array_merge($current, $leaf));
				} else {
					$this->selectedFilters->set($key, $leaf);
				}
			} else {
				$this->selectedFilters->set($key, $leaf);
			}
		}

		$this->cachePolicy = null;
	}

	/**
	 * @return Section[]
	 */
	public function getFilters()
    {
		return $this->filters;
	}

	/**
	 * @return Data
	 */
	public function getSelectedFilters()
    {
		return $this->selectedFilters;
	}

	/**
	 * @return Search
	 */
	public function getSearchModel()
    {
		$searchModel = new Search([
			'deploymentSiteIds' => $this->deploymentSiteIds
		]);

		$categoryIds = [];
		$folderIds = [];
		$propertyMap = [];
		foreach (self::$activeItems as $activeItem) {
			$options = $activeItem->getSection()->getOptions();
			if ($options instanceof CategoryOptions) {
				$categoryIds[] = $activeItem->getIdentifier();
			} elseif ($options instanceof FolderOptions) {
				$folderIds[] = $activeItem->getIdentifier();
			} elseif ($options instanceof PropertyOptions) {
				if (!isset($propertyMap[$activeItem->getIdentifier()])) {
					$propertyMap[$activeItem->getIdentifier()] = [];
				}

				if ($options->isHierarchical()) {
					//Add hierarchical values to media search only when a leaf is selected, or there are no more active items under the selected node
					if (
                        empty($activeItem->getItems()) || empty(array_filter($activeItem->getItems(), function (Item $ai) {
                            return $ai->isActive();
                        }))
                    ) {
						$identifier = $activeItem->getIdentifier();

						$mode = ($activeItem->getSection()->getOptions()->getMode() === BaseOptions::FILTER_UNION
							? PropertyCriteria::HIERARCHICAL_ANY
							: PropertyCriteria::HIERARCHICAL_ALL
						);

						$hierarchicalPath = [mb_strtolower(normalizer_normalize($activeItem->getTitle()))];
						while ($activeItem = $activeItem->getParent()) {
							array_unshift($hierarchicalPath, $activeItem->getTitle());
						}
					} else {
						continue;
					}

					$propertyMap[$identifier][$mode][] = $hierarchicalPath;
					continue;
				}

				$mode = ($activeItem->getSection()->getOptions()->getMode() === BaseOptions::FILTER_UNION
					? PropertyCriteria::OPERATOR_CONTAINS_ANY
					: PropertyCriteria::OPERATOR_CONTAINS_ALL
				);
				$propertyMap[$activeItem->getIdentifier()][$mode][] = mb_strtolower(normalizer_normalize($activeItem->getTitle()));
			} elseif ($options instanceof FreeTextOptions) {
				$searchModel->setFreeText($this->parseFreeTextQuery($options));
			}
		}

		if (!empty($categoryIds)) {
			$searchModel->setCategoryIds($categoryIds);
		} else {
			$searchModel->setCategoryIds($this->categoryIds);
		}

		if (!empty($folderIds)) {
			$searchModel->setFolderIds($folderIds);
			$searchModel->setFolderDepth(2 ** 53);
		}

		foreach ($propertyMap as $systemName => $properties) {
			foreach ($properties as $mode => $values) {
				$searchModel->addPropertyCriteria(
					(new PropertyCriteria())
						->setSystemName($systemName)
						->setOperator($mode)
						->setValue($values)
				);
			}
		}

		return $searchModel;
	}

	/**
	 * @param FreeTextOptions $options
	 * @return string
	 */
	private function parseFreeTextQuery(FreeTextOptions $options): string
	{
		preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $options->getQuery(), $matches);
		if (!isset($matches[0])) {
			return $options->getQuery();
		}

		$freeTextQuery = (implode(' ' . $options->getOperator() . ' ', array_map(function ($item) use ($options) {
			if (substr($item, 0, 1) === '"' && substr($item, -1) === '"') {
				return $item;
			} elseif (in_array(strtolower($item), [self::FREETEXT_AND, self::FREETEXT_OR], true)) {
				return $item;
			} else {
				switch ($options->getWildCardMode()) {
					case FreeTextOptions::WILDCARD_DONT_ADD:
						return $item;
					case FreeTextOptions::WILDCARD_ADD_AFTER:
						return $item . '*';
					case FreeTextOptions::WILDCARD_ADD_BEFORE:
						return '*' . $item;
					case FreeTextOptions::WILDCARD_ADD_BOTH:
						return '*' . $item . '*';
				}
			}
		}, $matches[0])));

		$searchReplacePattern = [
			'and and and' => 'and',
			'and or and' => 'or',
			'or or or' => 'or',
			'or and or' => 'and'
		];
		$freeTextQuery = str_ireplace(array_keys($searchReplacePattern), array_values($searchReplacePattern), $freeTextQuery);
		return $freeTextQuery;
	}

	/**
	 * @param BaseOptions $options
	 */
	public function addFilterByOptions(BaseOptions $options)
    {
		$cachePolicy = $this->cachePolicy ?? $this->randomCachePolicy();
		$items = [];
		if ($options instanceof CategoryOptions) {
			$this->categoryIds = $options->getCategoryIds();
			$items = $this->qbankApi->filters()->categories(
				implode(',', $this->categoryIds),
				implode(',', $this->deploymentSiteIds),
				false,
				$this->ignoreMediaIds,
				$cachePolicy
			);
		} elseif ($options instanceof PropertyOptions) {
			$items = $this->qbankApi->filters()->property(
				$options->getSystemName(),
				$options->isPreloadNames(),
				implode(',', $this->categoryIds),
				implode(',', $this->deploymentSiteIds),
				$options->isHierarchical(),
				false,
				$this->ignoreMediaIds,
				$cachePolicy
			);
		} elseif ($options instanceof FolderOptions) {
			$items = $this->qbankApi->filters()->folder(
				$options->getParentId(),
				implode(',', $this->categoryIds),
				implode(',', $this->deploymentSiteIds),
				false,
				$cachePolicy
			);
		} elseif ($options instanceof FreeTextOptions) {
			$this->selectedFilters->append('freetext', []);
			$items = $this->qbankApi->filters()->freetext(
				$options->getQuery(),
				implode(',', $this->deploymentSiteIds),
				$options->getOperator(),
				false,
				$cachePolicy
			);
		}

		$this->filters[] = new Filter\Section($options, $items, $this);
	}

	/**
	 * Adds filter options from a JSON file, to enable cache warmup for complex filters (see Dometic manuals)
	 *
	 * @param BaseOptions $options
	 * @param string $json
	 */
	public function addFilterByJson(BaseOptions $options, string $json)
    {
		// We need unpack the JSON and recreate each object as FilterItem
		// as they are turned to StdObject by the json_encoding/decoding
		$items = json_decode($json);
		$newItems = [];
		foreach ($items as $item) {
			$newItems[] = new FilterItem(['title' => $item->title]);
		}
		$this->filters[] = new Filter\Section($options, $newItems, $this);
	}

	/**
	 * @param BaseOptions $options
	 * @param FilterItem[] $filterItems
	 */
	public function addFilter(BaseOptions $options, array $filterItems)
    {
		$this->filters[] = new Filter\Section($options, $filterItems, $this);
	}

	private function randomCachePolicy()
    {
		return new CachePolicy(true, 3600 + mt_rand(-500, 500));
	}

	/**
	 * Adds category ids to enable skipping fetching categories via API
	 *
	 * @param array $categoryIds
	 */
	public function setCategoryIds(array $categoryIds)
    {
		$this->categoryIds = $categoryIds;
	}

	public function setCachePolicy(CachePolicy $cachePolicy)
    {
		$this->cachePolicy = $cachePolicy;
	}

	public function setIgnoreMediaIds()
    {
		$this->ignoreMediaIds = true;
	}
}
