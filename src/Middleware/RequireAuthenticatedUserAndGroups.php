<?php

namespace QBNK\FrontendComponents\Middleware;

use Psr\Container\ContainerInterface;
use Slim\Router;
use QBNK\QBank\API\Utilities\Model\DeploymentSiteResponse;
use QBNK\QBank\API\Utilities\QBankApi;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class RequireAuthenticatedUserAndGroups
{
    public static bool $redirect = true;
    public static array $defaultGroupIds = [];

    /** @var string */
    protected $constant;

    public function __construct(protected ContainerInterface $container, protected bool $allowUnauthenticated = false)
    {
        $this->container = $container;
        foreach (['', '_QBANK', '_SAML2'] as $check) {
            if (defined('QB_AUTH' . $check)) {
                $this->constant = 'QB_AUTH' . $check;
                break;
            }
        }

        if (null === $this->constant) {
            throw new \InvalidArgumentException('No settings for auth found.');
        }
    }

    public function __invoke(Request $request, RequestHandler $handler)
    {
        /** @var AdapterInterface $auth */
        $auth = $this->container->get(QB_AUTH_QBANK);

        if ($auth->isAuthenticated() || $this->allowUnauthenticated) {
            /** @var Identity $identity */
            $identity = $auth->getIdentity();
            $userGroupIds = [...self::$defaultGroupIds, ...array_keys($identity?->getGroups() ?? [])];
            /** @var DeploymentSiteResponse */
            $deploymentSite = $this->container->get(DeploymentSiteResponse::class);
            $allowedGroupIds = array_keys($deploymentSite->getUtilityPluginSettings()->getSiteSettings()->getUserGroups());
            if (!empty($userGroupIds) && !empty(array_intersect($userGroupIds, $allowedGroupIds))) {
                QBankApi::$clientUserGroupIds = $userGroupIds;
                QBankApi::$deploymentSiteId = $deploymentSite->getId();
                return $handler->handle($request);
            }
        }

        $auth->logout();

        if (!self::$redirect) {
            $response = new Response();
            return $response->withStatus(401);
        }

        $originalPath = $request->getUri()->getPath();
        if ($request->getUri()->getQuery()) {
            $originalPath .= '?' . $request->getUri()->getQuery();
        }

        /** @var Router $router */
        $router = $this->container->get(QB_ROUTER);
        $response = new Response();
        return $response->withHeader('Location', $router->pathFor(ROUTE_LOGIN, [], ['error' => 'User groups does not have access to this site.', 'next' => $originalPath]))->withStatus(302);
    }
}
