<?php

namespace QBNK\FrontendComponents\Middleware;

use Psr\Container\ContainerInterface;
use QBNK\FrontendComponents\Auth\Adapter\AdapterInterface;
use QBNK\FrontendComponents\Auth\Identity;
use QBNK\QBank\API\QBankApi;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use SlimSession\Helper;

class IPAutoLogin
{

	public function __construct(protected ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function __invoke(Request $request, Response $response, callable $next)
	{
		/** @var AdapterInterface $auth */
		$auth = $this->container->get(QB_AUTH);
		if (!$auth->isAuthenticated()) {
			$settings = $this->container->get(QB_SETTINGS)[QB_FRONTEND][QB_IP_AUTO_LOGIN];
			$remoteIp = $request->getServerParams()['REMOTE_ADDR'];
			$remoteIpIsIPV4 = filter_var($remoteIp, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);

			if ($remoteIpIsIPV4) {
				$valid = $this->validateIpAgainstWildCards($remoteIp, $this->filterWildcardIps($settings['allowedIps']));
				if ($valid) {
					$this->setSession();
				}
			} else if (in_array($remoteIp, $settings['allowedIps'], true)) {
				$this->setSession();
			}
		}

		return $next($request, $response);
	}

	private function validateIpAgainstWildCards($remoteIp, $array): bool
	{
		foreach ($array as $validIp) {
			$finalValidIp = str_replace('.*', '', $validIp);
			if (strpos($remoteIp, $finalValidIp) !== false) {
				return true;
			}
		}
		return false;
	}

	private function filterWildcardIps($ipArray): array
	{
		$filtered = [];
		foreach ($ipArray as $ip) {
			if (strpos($ip, '*') !== false) {
				$filtered[] = $ip;
			}
		}
		return $filtered;
	}

	private function setSession()
	{
		/** @var QBankApi $qbankApi */
		$qbankApi = $this->container->get(QB_QBANKAPI);

		$settings = $this->container->get(QB_SETTINGS)[QB_FRONTEND][QB_IP_AUTO_LOGIN];

		$identity = new Identity($settings['username'], []);
		$identity->setFirstName($settings['firstname']);
		$identity->setLastName($settings['lastname']);

		$identity->setQBankSessionId(
			$qbankApi->events()->session(
				$this->container->get(QB_SETTINGS)[QB_QBANKAPI]['sourceId'],
				uniqid($_SERVER['SERVER_NAME'], false),
				$_SERVER['REMOTE_ADDR'],
				$_SERVER['HTTP_USER_AGENT']
			)
		);

		/** @var Helper $session */
		$session = $this->container->get(QB_SESSION);
		$session->set('identity', serialize($identity));
	}
}
