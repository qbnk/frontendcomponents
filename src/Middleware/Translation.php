<?php

namespace QBNK\FrontendComponents\Middleware;

use Locale as Locale;
use Psr\Container\ContainerInterface;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class Translation
{

	public function __construct(private ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function __invoke(Request $request, Response $response, callable $next)
	{
		$settings = $this->container->get(QB_SETTINGS)[QB_FRONTEND][QB_FRONTEND_TRANSLATION];
		$directory = $settings['directory'];
		$domain = $settings['domain'];

		$locale = Locale::acceptFromHttp($request->getHeader('ACCEPT_LANGUAGE')[0]);
		$locale = $_COOKIE[QB_FRONTEND_LANG_COOKIE] ?? $locale;

		if (is_dir($directory) && defined('LC_MESSAGES')) {
			setlocale(LC_ALL, $locale);
			putenv("LC_ALL={$locale}");
			bindtextdomain($domain, $directory);
		}
		textdomain($domain);
		return $next($request, $response);
	}
}
