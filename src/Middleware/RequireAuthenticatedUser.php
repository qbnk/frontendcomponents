<?php

namespace QBNK\FrontendComponents\Middleware;

use Psr\Container\ContainerInterface;
use QBNK\FrontendComponents\Auth\Adapter\AdapterInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;
use Slim\Router;

class RequireAuthenticatedUser
{
    const ARG_REQUIRED_GROUPS = 'requiredGroups';

    public static bool $redirect = true;

    /** @var string */
    protected $constant;

    public function __construct(protected ContainerInterface $container)
    {
        $this->container = $container;
        foreach (['', '_QBANK', '_SAML2', '_OIDC'] as $check) {
            if (defined('QB_AUTH' . $check)) {
                $this->constant = 'QB_AUTH' . $check;
                break;
            }
        }

        if (null === $this->constant) {
            throw new \InvalidArgumentException('No settings for auth found.');
        }
    }

    public function __invoke(Request $request, RequestHandler $handler)
    {
        /** @var AdapterInterface $auth */
        $auth = $this->container->get(QB_AUTH_QBANK);

        if ($auth->isAuthenticated()) {
            return $handler->handle($request);
        }

        if (!self::$redirect) {
            $response = new Response();
            return $response->withStatus(401);
        }

        $originalPath = $request->getUri()->getPath();
        if ($request->getUri()->getQuery()) {
            $originalPath .= '?' . $request->getUri()->getQuery();
        }

        /** @var Router $router */
        $router = $this->container->get(QB_ROUTER);
        $response = new Response();
        return $response->withHeader('Location', $router->pathFor(ROUTE_LOGIN, [], ['error' => 'notloggedin', 'next' => $originalPath]))->withStatus(302);
    }
}
