<?php

namespace QBNK\FrontendComponents\Auth\Adapter;

use Exception;
use Psr\Container\ContainerInterface;
use QBNK\FrontendComponents\Auth\Identity;
use QBNK\FrontendComponents\Auth\Interface\AdapterInterface;
use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Credentials;
use QBNK\QBank\API\QBankApi;
use Slim\Interfaces\RouteCollectorProxyInterface;
use SlimSession\Helper;

class QBank implements AdapterInterface
{
	public const SESSION_IDENTITY = 'identity';

	public function __construct(private ContainerInterface $container, private Helper $session, private Credentials $credentials, private array $settings = [])
	{
	}

	/**
	 * @param string $returnTo
	 * @param array $params
	 * @param string $params['username']
	 * @param string $params['password']
	 * @return bool
	 */
	public function authenticate($returnTo = null, $params = [])
	{
		if (!isset($params['username'], $params['password'])) {
			return;
		}

		/** @var QBankApi $qbankApi */
		$qbankApi = $this->container->get(QBankApi::class);
		$qbankApi->updateCredentials($params['username'], $params['password']);
		try {
			$user = $qbankApi->accounts()->retrieveCurrentUser(new CachePolicy(false, 0));

			$identity = new Identity($user->getUserName(), ['user' => $user]);
			$identity->setFirstName($user->getFirstName());
			$identity->setLastName($user->getLastName());
			$identity->setQBankId($user->getId());
			foreach ($user->getGroups() as $group) {
				$identity->addGroup($group->getName(), $group->getId());
			}
			$identity->setQBankSessionId(
				$qbankApi->events()->session(
					$this->container->get(QB_SOURCE_ID),
					uniqid($_SERVER['SERVER_NAME'], false),
					$_SERVER['REMOTE_ADDR'],
					$_SERVER['HTTP_USER_AGENT'],
					$user->getId()
				)
			);

			$this->session->set(self::SESSION_IDENTITY, serialize($identity));
			return true;
		} catch (Exception $e) {
			$qbankApi->updateCredentials($this->credentials->getUsername(), $this->credentials->getPassword());
		}
		return false;
	}

	/**
	 * @return boolean
	 */
	public function isAuthenticated()
	{
		return ($this->getIdentity() instanceof Identity);
	}

	public function logout()
	{
		$this->session->delete(self::SESSION_IDENTITY);
	}

	/**
	 * @return Identity
	 */
	public function getIdentity()
	{
		if (!empty($this->session->get(self::SESSION_IDENTITY)) && ($identity = unserialize($this->session->get(self::SESSION_IDENTITY))) !== false) {
			return $identity;
		}

		return null;
	}

	/**
	 * @return void
	 */
	public function registerRoutes(RouteCollectorProxyInterface $app)
	{
		// void
	}
}
