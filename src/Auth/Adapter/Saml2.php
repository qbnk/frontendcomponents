<?php

namespace QBNK\FrontendComponents\Auth\Adapter;

use Exception;
use OneLogin\Saml2\Auth;
use OneLogin\Saml2\Error;
use OneLogin\Saml2\Utils;
use QBNK\FrontendComponents\Auth\Identity;
use QBNK\FrontendComponents\Auth\Interfaces\GetUserTypeInterface;
use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Model\Group;
use QBNK\QBank\API\Model\User;
use QBNK\QBank\API\QBankApi;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use QBNK\FrontendComponents\Auth\Interface\AdapterInterface;
use QBNK\QBank\API\Credentials;
use SlimSession\Helper;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface;
use Throwable;

class Saml2 implements AdapterInterface
{
    public const SESSION_IDENTITY = 'identity';
    public const SESSION_SAML_NAME_ID = 'samlNameId';
    public const SESSION_SAML_INDEX = 'samlSessionIndex';

    /**
     * @var array
     */
    private $getUserType;

    public function __construct(private ContainerInterface $container, private Helper $session, private Credentials $credentials, private array $settings = [], array $getUserType = [])
    {
        if (!empty($getUserType)) {
            $className = $getUserType[0];
            if ($className instanceof GetUserTypeInterface) {
                $this->getUserType = $getUserType;
            }
        }
    }

    /**
     * @param string $returnTo
     * @param array $params
     * @return void
     */
    public function authenticate($returnTo = null, $params = [])
    {
        $auth = new Auth($this->settings);
        $auth->login($returnTo);
        die;
    }

    /**
     * @return boolean
     */
    public function isAuthenticated()
    {
        return ($this->getIdentity() instanceof Identity);
    }

    public function logout()
    {
        $this->session->delete(self::SESSION_IDENTITY);
        $this->session->delete(self::SESSION_SAML_NAME_ID);
        $this->session->delete(self::SESSION_SAML_INDEX);
        $auth = new Auth($this->settings);
        try {
            $auth->logout();
        } catch (Error $e) {
        }
    }

    /**
     * @return Identity
     */
    public function getIdentity()
    {
        if (!empty($this->session->get(self::SESSION_IDENTITY)) && ($identity = unserialize($this->session->get(self::SESSION_IDENTITY))) !== false) {
            return $identity;
        }
    }

    /**
     * Optionally provide a prefix for the routes, in case you need multiple sets of routes
     * @return void
     */
    public function registerRoutes(RouteCollectorProxyInterface $app, string $routePrefix = '')
    {
        $me = $this;
        $routeNamePrefix = $routePrefix ? $routePrefix . '_' : '';
        $app->group('/' . ($routePrefix ? $routePrefix . '/' : '') . 'auth', function (RouteCollectorProxyInterface $app) use ($me, $routeNamePrefix) {
            $app->get('/metadata[.xml]', [$me, 'metadata'])->setName($routeNamePrefix . 'auth_metadata');
            $app->map(['GET', 'POST'], '/assertionConsumerService', [$me, 'assertionConsumerService'])->setName($routeNamePrefix . 'auth_acs');
            $app->get('/singleLogoutService', [$me, 'singleLogoutService'])->setName($routeNamePrefix . 'auth_slo');
            $app->get('/test', [$me, 'test'])->setName($routeNamePrefix . 'auth_test');
        });

        $routeParser = $app->getRouteCollector()->getRouteParser();
        $baseUrl = "https://" . str_replace(["https://", "https://"], "", $_SERVER['HTTP_HOST']);
        $this->settings['sp']['entityId'] = $baseUrl . $routeParser->urlFor($routeNamePrefix . 'auth_metadata');
        $this->settings['sp']['assertionConsumerService']['url'] = $baseUrl . $routeParser->urlFor($routeNamePrefix . 'auth_acs');
        $this->settings['sp']['singleLogoutService']['url'] = $baseUrl . $routeParser->urlFor($routeNamePrefix . 'auth_slo');
    }

    public function test(ServerRequestInterface $request, ResponseInterface $response, array $params = [])
    {
        if ($this->isAuthenticated()) {
            echo '<pre>';
            var_dump($this->getIdentity());
            die;
        } else {
            /** @var App */
            $app = $this->container->get(App::class);
            $router = $app->getRouteCollector();
            $returnTo = $router->getNamedRoute('auth_test');
            $this->authenticate($returnTo);
        }
    }

    public function metadata(ServerRequestInterface $request, ResponseInterface $response, array $params = [])
    {
        try {
            $auth = new Auth($this->settings);
            $settings = $auth->getSettings();
            $metadata = $settings->getSPMetadata();
            $errors = $settings->validateMetadata($metadata);
            if (empty($errors)) {
                $response->getBody()->write($metadata);
                return $response
                    ->withStatus(200)
                    ->withAddedHeader('Content-Type', 'text/xml');
            } else {
                throw new Error(
                    'Invalid SP metadata: ' . implode(', ', $errors),
                    Error::METADATA_SP_INVALID
                );
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            return $response
                ->withStatus(500, $e->getMessage());
        }
    }

    public function assertionConsumerService(ServerRequestInterface $request, ResponseInterface $response, array $params = [])
    {
        $qbankUserId = null;
        $auth = new Auth($this->settings);
        $auth->processResponse();
        $errors = $auth->getErrors();

        if (!empty($errors)) {
            error_log(implode(', ', $errors));
            error_log('Error reason: ' . $auth->getLastErrorReason());
            if ($auth->getLastErrorException() instanceof Throwable) {
                error_log('Exception: ' . $auth->getLastErrorException()->getMessage() . "\n" . $auth->getLastErrorException()->getTraceAsString());
            }
            die('An error occurred while processing SSO, please contact your system administrator.');
        }

        if (!$auth->isAuthenticated()) {
            die('Unable to log you in, please contact your system administrator.');
        }

        if (!empty($auth->getAttribute($this->settings['responseUsername'])[0])) {
            $userName = $auth->getAttribute($this->settings['responseUsername'])[0];
        } else {
            $userName = $auth->getNameId();
        }

        $identity = new Identity($userName, $auth->getAttributes());

        if (($firstName = $auth->getAttribute($this->settings['responseFirstname'])) !== null) {
            $identity->setFirstName($firstName[0]);
        }
        if (($lastName = $auth->getAttribute($this->settings['responseLastname'])) !== null) {
            $identity->setLastName($lastName[0]);
        }

        /** @var QBankApi $qbankApi */
        $qbankApi = $this->container->get(QB_QBANKAPI);

        $newGroupIds = [];
        if (!empty($this->settings['jit'])) {
            $adGroups = [];
            if (!empty($this->settings['jit']['responseGroups'])) {
                /** @var string[] $adGroups */
                $adGroups = $auth->getAttribute($this->settings['jit']['responseGroups']);
            }
            if (!empty($this->settings['jit']['groupMapping']) && is_array($this->settings['jit']['groupMapping']['qbankDefaultGroups'])) {
                $newGroupIds[] = $this->settings['jit']['groupMapping']['qbankDefaultGroups'];
            }

            foreach ($adGroups as $adGroup) {
                if (is_array($this->settings['jit']['groupMapping'][$adGroup])) {
                    $newGroupIds[] = $this->settings['jit']['groupMapping'][$adGroup];
                }
            }

            $newGroupIds = array_unique(array_merge([], ...$newGroupIds));
        }

        if (!empty($newGroupIds)) {
            foreach ($newGroupIds as $groupId) {
                $identity->addGroup($groupId);
            }
        }

        if ($this->settings['addQBankAccount'] === true || $this->settings['jit']['enabled'] === true) {
            $qbankUser = $this->addUserToQBank($qbankApi, $identity, $newGroupIds);
        }

        if ($qbankUser instanceof User) {
            $qbankUserId = $qbankUser->getId();

            $updateGroups = $this->settings['jit']['updateGroups'] ?? true; // Backwards compatability

            if ($this->settings['jit']['enabled'] === true && $updateGroups === true) {
                $currentGroupIds = array_map(static function (Group $group) {
                    return $group->getId();
                }, $qbankUser->getGroups() ?? []);

                $groupsToRemove = array_diff($currentGroupIds, $newGroupIds);
                $groupsToAdd = array_diff($newGroupIds, $currentGroupIds);

                if (!empty($groupsToRemove)) {
                    try {
                        $qbankApi->accounts()->removeUserFromGroup($qbankUserId, implode(',', $groupsToRemove));
                    } catch (\Throwable $t) {
                        // This will throw an error if used with version qbank3api-phpwrapper <3.1, just skip..
                    }
                }

                if (!empty($groupsToAdd)) {
                    $qbankApi->accounts()->addUserToGroup($qbankUserId, $groupsToAdd);
                }

                if (!empty($groupsToAdd) || !empty($groupsToRemove)) {
                    // Groups have been updated
                    $qbankUser = $qbankApi->accounts()->retrieveUser($qbankUserId, new CachePolicy(CachePolicy::OFF, 0));
                }
            }

            foreach ($qbankUser->getGroups() ?? [] as $group) {
                $identity->addGroup($group->getName(), $group->getId());
            }
        }

        $identity->setQBankSessionId(
            $qbankApi->events()->session(
                $this->container->get(QB_SOURCE_ID),
                uniqid($_SERVER['SERVER_NAME'], false),
                $_SERVER['REMOTE_ADDR'],
                $_SERVER['HTTP_USER_AGENT'],
                $qbankUserId
            )
        );

        $this->session->set(self::SESSION_IDENTITY, serialize($identity));
        $this->session->set(self::SESSION_SAML_NAME_ID, $auth->getNameId());
        $this->session->set(self::SESSION_SAML_INDEX, $auth->getSessionIndex());
        if (isset($_REQUEST['RelayState']) && Utils::getSelfURL() !== $_REQUEST['RelayState']) {
            $redirectTo = $_REQUEST['RelayState'];
        } else {
            $redirectTo = $this->container->get('request')->getUri()->getBaseUrl();
        }

        if (!empty($this->settings['includeUserDataCookie'])) { // For them SPA's
            $sessionSettings = $this->container->get(QB_SETTINGS)[QB_SESSION] ?? [];
            $cookieLifetime = $sessionSettings['lifetime'] ?? '1 hour';
            $displayName = !empty($firstName) && !empty($lastName) ? $firstName[0] . ' ' . $lastName[0] : $userName ?? '';
            setcookie(
                defined('QB_USER_DATA_COOKIE_NAME') ?
                QB_USER_DATA_COOKIE_NAME : 'user-data',
                json_encode([
                    'username' => $userName,
                    'displayName' => $displayName,
                    'groups' => $identity->getGroups()
                ]),
                strtotime($cookieLifetime),
                '/'
            );
        }

        return $response->withHeader('Location', $redirectTo)->withStatus(302);
    }

    protected function addUserToQBank(QBankApi $qbankApi, Identity $identity, array $groups): ?User
    {
        try {
            $user = $qbankApi->accounts()->retrieveUser($identity->getUsername(), new CachePolicy(CachePolicy::OFF, 0));
            if ($user instanceof User) {
                return $user;
            }
        } catch (Exception $e) {
            // User does not exist
        }

        $userType = null;
        if (isset($this->getUserType)) {
            $userType = call_user_func_array($this->getUserType, $groups) ?? '';
        }

        try {
            $user = $qbankApi->accounts()->createUser(
                (new User())
                    ->setUserName($identity->getUsername())
                    ->setEmail($identity->getUsername())
                    ->setFirstName($identity->getFirstName())
                    ->setLastName($identity->getLastName())
                    ->setGroups(array_map(fn ($id) => new Group(['id' => $id]), $identity->getGroups()))
                    ->setUserType($userType),
                null,
                null,
                false
            );

            return $user;
        } catch (Exception $e) {
            // Something went wrong while creating user in QBank, fallback to API user.
        }

        return null;
    }

    /**
     * Get the value of settings
     *
     * @return  array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Set the value of settings
     *
     * @param  array  $settings
     *
     * @return  self
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;
        return $this;
    }
}