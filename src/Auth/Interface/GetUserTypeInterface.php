<?php

namespace QBNK\FrontendComponents\Auth\Interfaces;

interface GetUserTypeInterface
{
    public static function getUserType(array $groups): string;
}
