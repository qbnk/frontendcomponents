<?php

namespace QBNK\FrontendComponents\Auth\Interface;

use Psr\Container\ContainerInterface;
use QBNK\FrontendComponents\Auth\Result;
use QBNK\QBank\API\Credentials;
use Slim\Interfaces\RouteCollectorProxyInterface;
use SlimSession\Helper;

interface AdapterInterface
{
	public function __construct(ContainerInterface $container, Helper $session, Credentials $credentials, array $settings = []);

	/**
	 * @param string $returnTo Path to redirect to after a successful login
	 * @param array $params
	 * @return bool true/false if login succeeded and there was no redirect
	 */
	public function authenticate($returnTo = null, $params = []);

	/**
	 * @return boolean
	 */
	public function isAuthenticated();

	/**
	 * @return void
	 */
	public function logout();

	/**
	 * @return Result
	 */
	public function getIdentity();

	/**
	 * @return void
	 */
	public function registerRoutes(RouteCollectorProxyInterface $app);
}
