<?php

namespace QBNK\FrontendComponents\Auth;

class Result
{
	const SUCCESS = 0;
	const FAILURE_CREDENTIAL_INVALID = 1;
}
