<?php

declare(strict_types=1);

namespace QBNK\FrontendComponents\Auth\Manager;

use DateTime;
use Exception;
use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Model\Group;
use QBNK\QBank\API\Model\User;
use QBNK\QBank\API\QBankApi;
use Throwable;

class AccountManager
{
	/**
	 * @var array $settings = [
	 *		'qbank-manager-group-ids' => int[],
	 *		'terms' => string,
	 *		'account-expire' => string ('+1 year'),
	 *		'apply-page-group-ids' => int[],
	 *		'approve-page-group-ids' => int[],
	 *		'custom-options' => [...]
	 *	]
	 */
	protected $settings;

	/** @var QBankApi */
	protected $qbankApi;

	public const DEFAULT_EXPIRE = '+1 year';

	public function __construct(QBankApi $qbankApi, array $settings)
    {
		$this->qbankApi = $qbankApi;
		$this->settings = $settings;
	}

	/**
	 * @return Group[]
	 */
	public function getApplyGroups(): array
    {
		$groupIds = $this->settings['apply-page-group-ids'] ?? [];
		if (empty($groupIds)) {
			return [];
		}

		return array_filter($this->qbankApi->accounts()->listGroups(), static function (Group $group) use ($groupIds) {
			return in_array($group->getId(), $groupIds, true);
		});
	}

	/**
	 * @return Group[]
	 */
	public function getApproveGroups(): array
    {
		$groupIds = array_unique(
			array_merge(
				$this->settings['apply-page-group-ids'] ?? [],
				$this->settings['approve-page-group-ids'] ?? []
            )
        );

		return array_filter($this->qbankApi->accounts()->listGroups(), static function (Group $group) use ($groupIds) {
			return in_array($group->getId(), $groupIds, true);
		});
	}

	/**
	 * @return string
	 */
	public function getTerms(): string
    {
		return $this->settings['terms'] ?? '';
	}

	/**
	 * @return array
	 */
	public function getCustomOptions(): array
    {
		return $this->settings['custom-options'] ?? [];
	}

	/**
	 * @param string $email
	 * @return bool
	 */
	public function validateAdminEmail(string $email): bool
    {
		$validGroupIds = $this->settings['qbank-manager-group-ids'] ?? [];
		if (empty($validGroupIds)) {
			return false;
		}

		try {
			$adminUser = $this->qbankApi->accounts()->retrieveUser($email);
			foreach ($adminUser->getGroups() as $group) {
				if (in_array($group->getId(), $validGroupIds, true)) {
					return true;
				}
			}
		} catch (Throwable $t) {
			// TODO: add debug logging
		}

		return false;
	}

	/**
	 * @param string $application
	 * @param string|null $key
	 * @return string
	 */
	public function saveAccountApplication(string $application, string $key = null): string
    {
		if ($key === null) {
			$key = md5($application);
		}

		$this->qbankApi->accounts()->createSetting($key, $application);
		return $key;
	}

	/**
	 * @param string $key
	 */
	public function removeAccountApplication(string $key): void
    {
		$this->qbankApi->accounts()->removeSetting($key);
	}

	/**
	 * @param string $key
	 * @return string
	 */
	public function getAccountApplication(string $key): string
    {
		return (string) $this->qbankApi->accounts()->retrieveSetting($key, new CachePolicy(CachePolicy::OFF, 0));
	}

	/**
	 * @param User $user
	 * @param array $groupIds
	 * @param string|null $redirectTo
	 * @param array $errors - By reference
	 * @return User with ID if created successfully
	 */
	public function addAccount(User $user, array $groupIds, string $redirectTo = null, &$errors = []): User
    {

		if ($this->settings['account-expire'] !== false) {
			try {
				$user->setEndDate(new \DateTime($this->settings['account-expire'] ?? self::DEFAULT_EXPIRE));
			} catch (Exception $e) {
				$errors[] = $e;
			}
		}

		try {
			$user = $this->qbankApi->accounts()->createUser($user, null, $redirectTo, true);
			if (!empty($groupIds)) {
				$this->qbankApi->accounts()->addUserToGroup($user->getId(), $groupIds);
			}
		} catch (Throwable $t) {
			$errors[] = $t;
		}

		return $user;
	}
}
