<?php

namespace QBNK\FrontendComponents\Auth;

class Identity
{
	/**
	 * @var int
	 */
	private $qbankId;
	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $firstName;

	/**
	 * @var string
	 */
	private $lastName;

	/**
	 * @var array array of groupId => groupName
	 */
	private $groups;

	/**
	 * @var array
	 */
	private $attributes;

	/**
	 * @var int
	 */
	private $qbankSessionId;

	/**
	 * Identity constructor.
	 * @param string $username
	 * @param array $attributes
	 */
	public function __construct($username, array $attributes)
    {
		$this->username = $username;
		$this->attributes = $attributes;
	}

	/**
	 * @return string
	 */
	public function getUsername()
    {
		return $this->username;
	}

	/**
	 * @param string $username
	 * @return Identity
	 */
	public function setUsername($username)
    {
		$this->username = $username;
		return $this;
	}

	/**
	 * @param $name
	 * @param mixed|null $groupId
	 * @return $this
	 */
	public function addGroup($name, $groupId = null)
    {
		if (null === $groupId) {
			$groupId = uniqid('group', true);
		}

		$this->groups[$groupId] = $name;
		return $this;
	}

	public function getGroups()
    {
		return $this->groups;
	}

	/**
	 * @param $id
	 * @return boolean
	 */
	public function hasGroupWithId($id)
    {
		return isset($this->groups[$id]);
	}

	/**
	 * @param string $name
	 * @param bool $strict
	 * @return bool
	 */
	public function hasGroupWithName($name, $strict = false)
    {
		return ($idx = array_search($name, $this->groups, $strict)) !== false;
	}

	/**
	 * @return array
	 */
	public function getAttributes()
    {
		return $this->attributes;
	}

	/**
	 * @param array $attributes
	 * @return Identity
	 */
	public function setAttributes($attributes)
    {
		$this->attributes = $attributes;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFirstName()
    {
		return $this->firstName;
	}

	/**
	 * @param string $firstName
	 * @return Identity
	 */
	public function setFirstName($firstName)
    {
		$this->firstName = $firstName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLastName()
    {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 * @return Identity
	 */
	public function setLastName($lastName)
    {
		$this->lastName = $lastName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDisplayName()
    {
		return $this->firstName . ' ' . $this->lastName;
	}

	/**
	 * @return int
	 */
	public function getQBankSessionId()
    {
		return $this->qbankSessionId;
	}

	/**
	 * Set the sessionId for this logged on user in QBank, this can then be used for other events, such as view, search and download
	 *
	 * @param int $qbankSessionId
	 * @return Identity
	 */
	public function setQBankSessionId($qbankSessionId)
    {
		$this->qbankSessionId = $qbankSessionId;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getQBankId()
    {
		return $this->qbankId;
	}

	/**
	 * @param int $qbankId
	 * @return $this
	 */
	public function setQBankId($qbankId)
    {
		$this->qbankId = $qbankId;
		return $this;
	}
}
