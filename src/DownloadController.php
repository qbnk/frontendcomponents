<?php

namespace QBNK\FrontendComponents;

use Exception;
use InvalidArgumentException;
use LogicException;
use QBNK\QBank\API\Exception\NotFoundException;
use QBNK\QBank\API\Model\DownloadItem;
use QBNK\QBank\API\Model\MediaResponse;
use QBNK\QBank\API\Model\MimeType;
use QBNK\QBank\API\QBankApi;
use RuntimeException;
use SplFileInfo;

class DownloadController
{
	/** @var array Array holding all files to be downloaded */
	protected $files;

	/** @var QBankApi */
	protected $qbankApi;

	/** @var array */
	private $downloadTemplates;

	/** @var array */
	private $nonFallbackCategories;

	/** @var bool */
	private $presentPdf;

	public function __construct(QBankApi $qbankApi)
	{
		$this->qbankApi = $qbankApi;
		$this->presentPdf = false;
		$this->downloadTemplates = [
			MimeType::CLASSIFICATION_IMAGE => [],
			MimeType::CLASSIFICATION_VIDEO => [],
			MimeType::CLASSIFICATION_AUDIO => [],
			MimeType::CLASSIFICATION_DOCUMENT => [],
		];
		$this->nonFallbackCategories = [];
	}

	/**
	 * Adds categories to a list of categories that will not fall back on original (ie, user must always select a template)
	 *
	 * @param int $categoryId
	 */
	public function addNonFallbackCategory(int $categoryId): void
	{
		if (!in_array($categoryId, $this->nonFallbackCategories, true)) {
			$this->nonFallbackCategories[] = $categoryId;
		}
	}

	/**
	 * This method is an alias for DownloadController::addDownloadTemplate()
	 *
	 * Sets the download template for a specific classification
	 * If a category id is set, these templates will only be downloaded for media in that specific category
	 * Returns false if the classification is unavailable or if the template is already set
	 *
	 * @param string $classification
	 * @param int $templateId
	 * @param int $categoryId
	 * @return bool
	 * @deprecated since v3.2.1 - use DownloadController::addDownloadTemplate() instead
	 * @see addDownloadTemplate()
	 */
	public function setDownloadTemplate(string $classification, ?int $templateId, ?int $categoryId = null): bool
	{
		return $this->addDownloadTemplate($classification, $templateId, $categoryId);
	}

	/**
	 * Sets the download templates for a specific classification
	 * If a category id is set, these templates will only be downloaded for media in that specific category
	 * Returns false if the classification is unavailable (but always falls back to original template)
	 *
	 * @param string $classification
	 * @param int $templateId
	 * @param int $categoryId
	 * @return bool
	 */
	public function addDownloadTemplate(string $classification, ?int $templateId, ?int $categoryId = null): bool
	{
		if ($templateId === 0) {
			$templateId = null;
		}

		if (array_key_exists($classification, $this->downloadTemplates)) {
			if (!isset($this->downloadTemplates[$classification][$categoryId])) {
				$this->downloadTemplates[$classification][$categoryId] = [];
			}
			if ($categoryId && !in_array($templateId, $this->downloadTemplates[$classification][$categoryId], true)) {
				$this->downloadTemplates[$classification][$categoryId][] = $templateId;
			} elseif (!$categoryId && !in_array($templateId, $this->downloadTemplates[$classification], true)) {
				$this->downloadTemplates[$classification][] = $templateId;
			}
			return true;
		}
		return false;
	}

	/**
	 * Downloads multiple media into a zip file
	 *
	 * @param MediaResponse[] $media
	 * @param int $deploymentSiteId
	 * @param int $sessionId
	 * @param string|null $zipFileName
	 * @param string $deploymentPath
	 * @param bool $debug
	 * @throws InvalidArgumentException If the given array of media does not contain MediaResponse objects or numeric ids
	 * @throws NotFoundException When a media cannot be retrieved or a deployed file cannot be fetched
	 * @throws Exception If a file cannot be added for downloading (usually when the physical file does not exist) or the zip could not be created
	 */
	public function downloadMultiple(array $media, int $deploymentSiteId, $sessionId, string $zipFileName = null, $deploymentPath = '', $debug = false): void
	{
		$downloads = [];
		foreach ($media as $medium) {
			if (!($medium instanceof MediaResponse)) {
				if (!is_numeric($medium)) {
					throw new InvalidArgumentException('$media must be MediaResponse[] or a numeric array');
				}
				$medium = $this->qbankApi->media()->retrieveMedia((int)$medium);
			}
			$categoryId = $medium->getCategoryId();
			$classification = $medium->getMimetype()->getClassification();
			$templateType = null;
			switch ($classification) {
				case MimeType::CLASSIFICATION_IMAGE:
					$templateType = 'imageTemplateId';
					break;
				case MimeType::CLASSIFICATION_VIDEO:
					$templateType = 'videoTemplateId';
					break;
				case MimeType::CLASSIFICATION_AUDIO:
					$templateType = 'audioTemplateId';
					break;
				case MimeType::CLASSIFICATION_DOCUMENT:
					$templateType = 'documentTemplateId';
					break;
			}

			$downloadTemplates = $this->getDownloadTemplatesByClassification($classification, $categoryId);
			// If the classification has no set download templates, fall back to original (null) but only if the category allows it
			if (empty($downloadTemplates) && !in_array($categoryId, $this->nonFallbackCategories, true)) {
				$downloadTemplates = [null];
			}

			if (empty($downloadTemplates)) {
				continue;
			}

			foreach ($downloadTemplates as $downloadTemplate) {
				$deployedFile = $medium->getDeployedFile($downloadTemplate, $classification, $deploymentSiteId);

				$downloadItem = ['mediaId' => $medium->getMediaId()];
				if ($templateType !== null) {
					$downloadItem[$templateType] = $downloadTemplate;
				}
				$downloads[] = new DownloadItem($downloadItem);

				$remoteFile = $deployedFile->getRemoteFile();
				$filename = pathinfo($medium->getFilename(), PATHINFO_FILENAME) . '.' . pathinfo($remoteFile, PATHINFO_EXTENSION);
				$this->addFile(new SplFileInfo($deploymentPath . $remoteFile), $filename);
			}
		}

		if ($debug) {
			echo 'Would download ' . count($downloads) . ' files into <strong>image_bank_' . date('Ymd') . '.zip</strong>';
			return;
		}

		try {
			$this->qbankApi->events()->download($sessionId, $downloads);
		} catch (Exception $e) {

		}
		$this->download($zipFileName);
	}

	/**
	 * Gets an array of download template ids for a specific classification and category id
	 *
	 * @param string $classification
	 * @param int $categoryId
	 * @return array
	 */
	public function getDownloadTemplatesByClassification(string $classification, int $categoryId): array
	{
		if (array_key_exists($classification, $this->downloadTemplates)) {
			if ($categoryId && isset($this->downloadTemplates[$classification][$categoryId]) && !empty($this->downloadTemplates[$classification][$categoryId])) {
				return $this->downloadTemplates[$classification][$categoryId];
			} elseif (!$categoryId && !empty($this->downloadTemplates[$classification])) {
				return $this->downloadTemplates[$classification];
			}
		}
		return [];
	}

	/**
	 * Adds a file to the download list.
	 *
	 * @param SplFileInfo $file The file that should be added to the download list
	 * @param string $filename The pretty filename that the file should get, note that when downloading a zip you are
	 * responsible to make sure all filenames are unique.
	 * @throws RuntimeException
	 */
	public function addFile(SplFileInfo $file, $filename): void
	{
		if (!$file->isReadable()) {
			throw new RuntimeException(sprintf('File "%s" could not be read.', $file->getFilename()));
		}
		$this->files[] = [
			'filename' => $filename,
			'file' => $file
		];
	}

	/**
	 * Starts a download of added files {@see addFile}
	 *
	 * @param string $zipFilename Sets the filename of archive downloads, if not given a random one will be used
	 * @throws LogicException If no files have been added to the download list
	 * @throws RuntimeException If the temporary directory before packing a zip cannot be created
	 */
	public function download($zipFilename = null): void
	{

		$this->setSensiblePhpDefaults();

		if (empty($this->files)) {
			throw new LogicException('No files to download.');
		}

		if (count($this->files) > 1) {
			if ($zipFilename === null) {
				$zipFilename = uniqid('qbank', true) . '.zip';
			}
			$this->downloadZip($zipFilename);
		} else {
			$file = $this->files[0];
			$this->downloadFile($file['file'], $file['filename']);
		}
	}

	private function setSensiblePhpDefaults(): void
	{
		ob_end_clean();
		@set_time_limit(0);
		@error_reporting(0);
		@ini_set('zlib.output_compression', 0);
	}

	/**
	 * @param $zipFilename
	 * @throws RuntimeException
	 */
	private function downloadZip($zipFilename): void
	{
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $zipFilename . '";');
		header('Content-Transfer-Encoding: binary');

		$tempDirectory = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid($zipFilename, true);

		if (!@mkdir($tempDirectory) && !is_dir($tempDirectory)) {
			throw new RuntimeException('Unable to create dir ' . $tempDirectory);
		}

		register_shutdown_function(function () use ($tempDirectory) {
			foreach (glob($tempDirectory . '/*') as $file) {
				unlink($file);
			}
			rmdir($tempDirectory);
		});

		$descriptors = [['pipe', 'r'], ['pipe', 'w']];
		$process = proc_open('zip -0 -r -j -@ -', $descriptors, $pipes, '', []);
		if (is_resource($process)) {
			[$stdin, $stdout] = $pipes;

			$estimatedSize = 0;
			foreach ($this->files as $file) {
				$estimatedSize += filesize($file['file']->getRealPath());
				`ln -s "{$file['file']->getRealPath()}" "{$tempDirectory}/{$file['filename']}"`;
				fwrite($stdin, $tempDirectory . DIRECTORY_SEPARATOR . $file['filename'] . PHP_EOL);
			}
			fclose($stdin);

			if ($fout = fopen('php://output', 'w')) {
				stream_copy_to_stream($stdout, $fout);
				fclose($fout);
			}

			proc_close($process);
		}
	}

	private function downloadFile(SplFileInfo $file, $filename): void
	{
		$browser = new \Browser();

		if ($this->presentPdf) {
			header('Content-type: application/pdf');
			header('Content-Transfer-Encoding: binary');
			header('Content-Disposition: inline; filename="' . $filename . '";');
			header('Accept-Ranges: bytes');
		} else {
			header('Content-Type: application/octet-stream');

			if (in_array($browser->getBrowser(), [\Browser::BROWSER_EDGE, \Browser::BROWSER_IE], true)) {
				header('Content-Disposition: attachment; filename="' . urlencode(str_replace(' ', '_', $filename)) . '";');
			} else {
				header('Content-Disposition: attachment; filename="' . $filename . '";');
			}
		}

		header('Content-Length: ' . filesize($file->getRealPath()));

		if (false !== stripos($_SERVER['SERVER_SOFTWARE'], 'apache')) {
			header('X-SendFile: ' . $file->getRealPath());
		} elseif (false !== stripos($_SERVER['SERVER_SOFTWARE'], 'nginx')) {
			header('X-Accel-Redirect: ' . $file->getRealPath());
		}

		die;
	}

	public function prepareToPresentPdf(): DownloadController
	{
		$this->presentPdf = true;
		return $this;
	}
}
