<?php

namespace QBNK\FrontendComponents;

use QBNK\QBank\API\Exception\RequestException;
use QBNK\QBank\API\Exception\ResponseException;
use QBNK\QBank\API\QBankApi;
use QBNK\QBank\API\CachePolicy;

class SelectionController
{
	/**
	 * @var QBankApi
	 */
	protected $qbankApi;

	const SAVE_SESSION = 1;
	const SAVE_COOKIE = 2;
	const SAVE_SETTING = 3;

	protected $saveType;

	const SESSION_KEY = 'frontendselection';
	const COOKIE_KEY = 'frontendselectioncookie';

	/**
	 * A week
	 */
	const COOKIE_EXPIRE = 604800;

	/**
	 * SelectionController constructor.
	 * @param QBankApi $qbankApi
	 * @param int $saveType Where to save the selections
	 */
	public function __construct(QBankApi $qbankApi, $saveType = self::SAVE_SESSION)
    {
		$this->qbankApi = $qbankApi;
		$this->saveType = $saveType;
	}

	public function getSelection($hash = null, CachePolicy $cachePolicy = null)
    {
		if ($hash !== null) {
			return $this->getStoredSelection($hash, $cachePolicy);
		}

		if ($this->saveType === self::SAVE_SESSION && isset($_SESSION[$this::SESSION_KEY])) {
			return unserialize(gzuncompress($_SESSION[$this::SESSION_KEY]));
		}

		if ($this->saveType === self::SAVE_COOKIE && isset($_COOKIE[$this::COOKIE_KEY])) {
			return unserialize(gzuncompress($_COOKIE[$this::COOKIE_KEY]));
		}

		if ($this->saveType === self::SAVE_SETTING) {
			die('implement me :D');
		}

		return [];
	}

	public function addToSelection($mediaIds)
    {
		$selection = $this->getSelection();
		foreach ($mediaIds as $mediaId) {
			$selection[$mediaId] = true;
        }

		return $this->saveSelection($selection);
	}

	/**
	 * Overrides current selection and sets selection to provided mediaID's
	 * @param int[] $mediaIds
	 * @param int $timestamp
	 * @return mixed
	 * @throws RequestException
	 * @throws ResponseException
	 */
	public function setSelection($mediaIds, $timestamp)
    {
		$currentSelection = $this->getSelection();
		if (array_key_exists('updated', $currentSelection) && $currentSelection['updated'] > $timestamp) {
			return $currentSelection;
		}
		$selection = [];
		$selection['updated'] = $timestamp;
		$selection['mediaIds'] = [];
		foreach ($mediaIds as $mediaId) {
			$selection['mediaIds'][$mediaId] = true;
        }

		return $this->saveSelection($selection);
	}

	public function removeFromSelection($mediaIds)
    {
		$selection = $this->getSelection();
		foreach ($mediaIds as $mediaId) {
            if (isset($selection[$mediaId])) {
                unset($selection[$mediaId]);
            }
        }

		return $this->saveSelection($selection);
	}

	public function toggleInSelection($mediaId)
    {
		$selection = $this->getSelection();
		if (isset($selection[$mediaId])) {
			unset($selection[$mediaId]);
		} else {
            $selection[$mediaId] = true;
        }

		return $this->saveSelection($selection);
	}

	public function storeSelection($selection, $bucketId = "")
    {
		$key = $bucketId? md5(json_encode($selection)).'_'.$bucketId : md5(json_encode($selection));
		$this->qbankApi->accounts()->createSetting($key, join(',', array_keys($selection)));

		return $key;
	}

	public function saveSelection($selection)
    {
		if ($this->saveType === self::SAVE_SESSION) {
			$_SESSION[$this::SESSION_KEY] = gzcompress(serialize($selection));
		} elseif ($this->saveType === self::SAVE_COOKIE) {
			$_COOKIE[$this::COOKIE_KEY] = gzcompress(serialize($selection));
			setcookie($this::COOKIE_KEY, gzcompress(serialize($selection)), time() + $this::COOKIE_EXPIRE, '/');
		} elseif ($this->saveType === self::SAVE_SETTING) {
			die('implement me :D');
		}

		return $selection;
	}

	protected function getStoredSelection($hash, CachePolicy $cachePolicy = null)
    {
		$mediaIds = $this->qbankApi->accounts()->retrieveSetting($hash, false, $cachePolicy);
		if (!empty($mediaIds)) {
			$mediaIds = explode(',', $mediaIds);
			return array_fill_keys($mediaIds, true);
		}

		return [];
	}
}
