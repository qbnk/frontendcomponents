<?php

namespace QBNK\FrontendComponents\Filter;

class BaseOptions
{
	const FILTER_INTERSECT = 'intersect';
	const FILTER_UNION = 'union';

	/** Sort filter items alphanumerically in ascending order */
	const SORT_ALPHANUMERIC_ASC = 'alpha_asc';

	/** Sort filter items alphanumerically in descending order */
	const SORT_ALPHANUMERIC_DESC = 'alpha_desc';

	/** @var string */
	protected $title;

	/** @var string */
	protected $mode;

	/** @var callable|string */
	protected $sort;

	/** @var boolean */
	protected $resets;

	/** @var boolean */
	protected $resetsAll;

	/** @var boolean */
	protected $showAllOption;

	/**
	 * BaseOptions constructor.
	 * @param string $title
	 * @param boolean $resets
	 * @param boolean $resetsAll
	 * @param string $mode
	 * @param callable|string $sort
	 */
	public function __construct($title, $resets, $resetsAll, $mode = self::FILTER_INTERSECT, $sort = self::SORT_ALPHANUMERIC_ASC)
    {
		$this->title = $title;
		$this->resets = $resets;
		$this->resetsAll = $resetsAll;
		$this->mode = $mode;
		$this->sort = $sort;

		$this->showAllOption = true;
	}

	/**
	 * @return string
	 */
	public function getTitle()
    {
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function setTitle($title)
    {
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMode()
    {
		return $this->mode;
	}

	/**
	 * @param string $mode
	 * @return $this
	 */
	public function setMode($mode)
    {
		$this->mode = $mode;
		return $this;
	}

	/**
	 * @return callable|string
	 */
	public function getSort()
    {
		return $this->sort;
	}

	/**
	 * @param callable|string $sort
	 * @return $this
	 */
	public function setSort($sort)
    {
		$this->sort = $sort;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isResets()
    {
		return $this->resets;
	}

	/**
	 * @param bool $resets
	 * @return $this
	 */
	public function setResets($resets)
    {
		$this->resets = $resets;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isResetsAll()
    {
		return $this->resetsAll;
	}

	/**
	 * @param bool $resetsAll
	 * @return $this
	 */
	public function setResetsAll($resetsAll)
    {
		$this->resetsAll = $resetsAll;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isShowAllOption()
    {
		return $this->showAllOption;
	}

	/**
	 * @param bool $showAllOption
	 * @return $this
	 */
	public function setShowAllOption($showAllOption)
    {
		$this->showAllOption = $showAllOption;
		return $this;
	}
}
