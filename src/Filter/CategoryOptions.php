<?php

namespace QBNK\FrontendComponents\Filter;

class CategoryOptions extends BaseOptions
{
	/** @var int[] */
	protected $categoryIds;

	/**
	 * CategoryOptions constructor.
	 * @param string $title
	 * @param \int[] $categoryIds
	 * @param string $mode
	 * @param callable|string $sort
	 * @param bool $resets
	 * @param bool $resetsAll
	 */
	public function __construct($title, array $categoryIds, $mode = self::FILTER_INTERSECT, $sort = self::SORT_ALPHANUMERIC_ASC, $resets = true, $resetsAll = true)
    {
		parent::__construct($title, $resets, $resetsAll, $mode, $sort);
		$this->categoryIds = filter_var_array($categoryIds, FILTER_VALIDATE_INT);
	}

	/**
	 * @return \int[]
	 */
	public function getCategoryIds()
    {
		return $this->categoryIds;
	}

	/**
	 * @param \int[] $categoryIds
	 * @return $this
	 */
	public function setCategoryIds($categoryIds)
    {
		$this->categoryIds = filter_var_array($categoryIds, FILTER_VALIDATE_INT);
		return $this;
	}

	/**
	 * @param int $categoryId
	 * @throws \InvalidArgumentException
	 */
	public function addCategoryId($categoryId)
    {
		if (!is_int($categoryId)) {
			throw new \InvalidArgumentException('$categoryId must be integer.');
		}

		$this->categoryIds[] = $categoryId;
	}
}
