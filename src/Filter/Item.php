<?php

namespace QBNK\FrontendComponents\Filter;

use Dflydev\DotAccessData\Data;
use QBNK\FrontendComponents\FilterController;
use QBNK\QBank\API\Model\FilterItem;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

/**
 * Class Item
 * @package QBNK\FrontendComponents\Filter
 */
class Item
{
	/** @var string */
	protected $title;

	/** @var mixed */
	protected $identifier;

	/** @var boolean */
	protected $active;

	/** @var int[] */
	protected $mediaIds;

	/** @var Section */
	protected $section;

	/** @var Item[] */
	protected $items;

	/** @var string[] */
	protected $path;

	/** @var Item */
	protected $parent;

	/** @var int */
	protected $total;

	/**
	 * @param FilterItem $item
	 * @param Section $section
	 * @param Item|null $parent
	 * @param Data $selectedFilters
	 * @param array $path
	 * @return Item
	 */
	public static function fromFilterItem(FilterItem $item, Section $section, Item $parent = null, Data $selectedFilters, array $path = [])
    {
		$self = new static();
		if ($section->getOptions() instanceof PropertyOptions) {
			$self->identifier = $section->getOptions()->getSystemName();
		} else {
			$self->identifier = $item->getId();
		}
		$self->section = $section;
		$self->parent = $parent;
		$self->title = $item->getTitle();
		$self->mediaIds = array_combine($item->getMediaIds(), array_fill(0, count($item->getMediaIds()), true));
		$self->total = count($self->mediaIds);

		$urlSafeTitle = self::urlSafeTitle($self->title);

		$path[] = $urlSafeTitle;
		$self->path = $path;
		if ($selectedFilters->has($urlSafeTitle) || $selectedFilters->has(urldecode($urlSafeTitle))) {
			FilterController::$activeItems[] = $self;
			$self->active = true;
			$selectedFilters = new Data($selectedFilters->get($urlSafeTitle));
		} else {
			$selectedFilters = new Data();
		}

		if (!empty($item->getFilterItems())) {
			$sortedItems = $section->sortFilterItems($item->getFilterItems());
			$self->items = array_combine(
				array_map(function ($item) {
                    return Item::urlSafeTitle($item->getTitle());
                }, $sortedItems),
				array_map(function ($item) use ($section, $self, $selectedFilters) {
					return Item::fromFilterItem($item, $section, $self, $selectedFilters, $self->path);
				}, $sortedItems)
			);
		}

		return $self;
	}

	/**
	 * @param Section $section
	 * @param boolean $active
	 * @param array $mediaIds
	 * @return static
	 */
	public static function allItem(Section $section, $active, $mediaIds)
    {
		$self = new static();
		$self->section = $section;
		$self->title = 'All';
		$self->path = [];
		$self->active = $active;
		$self->mediaIds = $mediaIds;
		$self->total = count($mediaIds);

		return $self;
	}

	/**
	 * @return string
	 */
	public function getTitle()
    {
		return $this->title;
	}

	/**
	 * @return bool
	 */
	public function isActive()
    {
		return $this->active;
	}

	/**
	 * @return Section
	 */
	public function getSection()
    {
		return $this->section;
	}

	/**
	 * @return Item[]
	 */
	public function getItems()
    {
		return $this->items;
	}

	/**
	 * @return int
	 */
	public function getTotal()
    {
		return $this->total;
	}

	/**
	 * @return mixed
	 */
	public function getIdentifier()
    {
		return $this->identifier;
	}

	public function getCount(): int
    {
		$activeItems = FilterController::$activeItems;
		// $activeMediaIds = array_map(function(Item $item) { return $item->mediaIds; }, $activeItems);
		$activeMediaIds = [];
		foreach ($activeItems as $item) {
			if ($item->getSection()->getOptions() instanceof CategoryOptions) {
				$activeMediaIds[] = $item->getMediaIds();
			} else {
				if ($this->getSection() === $item->getSection()) {
					if (
						$this->getSection()->isResets() === false &&
						$this->getSection()->getOptions()->getMode() === PropertyOptions::FILTER_INTERSECT
					) {
						$activeMediaIds[] = $item->getMediaIds();
					}
				} else {
					if ($item->section->getOptions()->getMode() === PropertyOptions::FILTER_UNION) {
						$activeMediaIds[$item->getIdentifier()] = array_replace($activeMediaIds[$item->getIdentifier()] ?? [], $item->getMediaIds());
					} else {
						$activeMediaIds[] = $item->getMediaIds();
					}
				}
			}
		}

		/** @var array $activeMediaIds */
		$activeMediaIds = array_values($activeMediaIds); // reset keys in case of string keys due to union filters

		if (count($activeMediaIds) > 1) {
			$filteredMediaIds = array_intersect_key(...$activeMediaIds);
		} elseif (count($activeMediaIds) === 1) {
			$filteredMediaIds = $activeMediaIds[0];
		} else {
			$filteredMediaIds = $this->mediaIds;
		}

		return count(array_intersect_key($filteredMediaIds, $this->mediaIds));
	}

	public function getLink()
    {
		$options = $this->section->getOptions();
		$resets = $options->isResets();
		$resetsAll = $options->isResetsAll();
		$hierarchical = ($options instanceof PropertyOptions && $options->isHierarchical());
		$selectedFilters = clone $this->section->getSelectedFilters();
		$urlSafeTitle = self::urlSafeTitle($this->title);
		$path = implode('.', $this->path);
		if ($resetsAll) {
			$selectedFilters = new Data();
			if (!empty($path)) {
				if ($this->active) {
					$selectedFilters->remove($path);
				} else {
					$selectedFilters->append($path, $path);
				}
			}
		} else {
			if (empty($path)) {
				$items = $this->section->getItems();
				array_map(function (Item $item) use ($selectedFilters) {
					if (!empty(array_filter($item->path))) {
						$selectedFilters->remove(implode('.', $item->path));
					}
				}, $items);
			} else {
				if ($this->active) {
					$selectedFilters->remove($path);
				} else {
					if ($resets) {
						array_map(function (Item $item) use ($selectedFilters) {
							$path = implode('.', $item->path);
							if (!empty($path)) {
								$selectedFilters->remove($path);
							}
						}, $this->section->getItems());
					}

					if ($selectedFilters->has($path)) {
						$selectedFilters->append($path, $urlSafeTitle);
					} else {
						$selectedFilters->set($path, $urlSafeTitle);
					}
				}
			}
		}

		return $this->buildLink($selectedFilters->export(), $hierarchical);
	}

	protected function buildLink(array $selectedFilters, $hierarchical = false)
    {

		$separators = FilterController::$separators;
		$paths = [];

		if ($hierarchical) {
			$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($selectedFilters), RecursiveIteratorIterator::SELF_FIRST);
			foreach ($iterator as $key => $value) {
				$collectedNodes = [$key => []];

				for ($path = [], $i = 0, $z = $iterator->getDepth(); $i < $z; $i++) {
					$path[] = $iterator->getSubIterator($i)->key();
				}

				$pathKey = implode($separators['hierarchicalSeparator'], $path);
				if ($pathKey !== '') {
					$paths[$pathKey][] = $key;
				} else {
					$collectedNodes[$key][] = $key;
				}
			}

			if ($collectedNodes !== null) {
				$paths += $collectedNodes;
			}
		} else {
			$iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($selectedFilters), RecursiveIteratorIterator::LEAVES_ONLY);
			$paths = [];
			foreach ($iterator as $key => $value) {
				for ($path = [], $i = 0, $z = $iterator->getDepth(); $i < $z; $i++) {
					$path[] = $iterator->getSubIterator($i)->key();
				}
				$paths[implode($separators['hierarchicalSeparator'], $path)][] = $key;
			}
		}

		$link = [];
		foreach ($paths as $key => $values) {
			if (!empty($values)) {
				$link[] = implode($separators['hierarchicalSeparator'], array_unique(array_filter([$key , implode($separators['hierarchicalLeafSeparator'], array_filter($values))])));
			}
		}

		return implode($separators['propertySeparator'], $link);
	}

	/**
	 * @param $title
	 * @return string
	 */
	public static function urlSafeTitle($title)
    {
		return mb_strtolower(rawurlencode(mb_strtolower($title)));
	}

	/**
	 * @return Item
	 */
	public function getParent()
    {
		return $this->parent;
	}

	/**
	 * @return int[]
	 */
	public function getMediaIds()
    {
		return $this->mediaIds;
	}

	/**
	 * @param int[] $mediaIds
	 */
	public function setMediaIds($mediaIds)
    {
		$this->mediaIds = $mediaIds;
	}
}
