<?php

namespace QBNK\FrontendComponents\Filter;

class PropertyOptions extends BaseOptions
{
	/** @var string */
	protected $systemName;

	/** @var boolean */
	protected $preloadNames;

	/** @var boolean */
	protected $isHierarchical;

	/**
	 * PropertyOptions constructor.
	 * @param string $title
	 * @param string $systemName
	 * @param bool $preloadNames
	 * @param bool $isHierarchical
	 * @param boolean $resets
	 * @param string $mode
	 * @param callable|string $sort
	 */
	public function __construct($title, $systemName, $preloadNames = false, $isHierarchical = false, $resets = false, $mode = self::FILTER_INTERSECT, $sort = self::SORT_ALPHANUMERIC_ASC)
    {
		parent::__construct($title, $resets, false, $mode, $sort);
		$this->systemName = $systemName;
		$this->preloadNames = $preloadNames;
		$this->isHierarchical = $isHierarchical;
	}

	/**
	 * @return string
	 */
	public function getSystemName()
    {
		return $this->systemName;
	}

	/**
	 * @param string $systemName
	 * @return $this
	 */
	public function setSystemName($systemName)
    {
		$this->systemName = $systemName;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isPreloadNames()
    {
		return $this->preloadNames;
	}

	/**
	 * @param bool $preloadNames
	 * @return $this
	 */
	public function setPreloadNames($preloadNames)
    {
		$this->preloadNames = $preloadNames;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isHierarchical()
    {
		return $this->isHierarchical;
	}

	/**
	 * @param bool $isHierarchical
	 * @return $this
	 */
	public function setIsHierarchical($isHierarchical)
    {
		$this->isHierarchical = $isHierarchical;
		return $this;
	}
}
