<?php

namespace QBNK\FrontendComponents\Filter;

use Dflydev\DotAccessData\Data;
use QBNK\FrontendComponents\FilterController;
use QBNK\QBank\API\Model\FilterItem;

class Section
{
	/** string */
	protected $title;

	/** @var BaseOptions */
	protected $options;

	/** @var Item[] */
	protected $items;

	/** @var FilterController */
	protected $filterController;

	/** @var bool */
	protected $isResets;

	/**
	 * Section constructor.
	 * @param BaseOptions $options
	 * @param FilterItem[] $items
	 * @param FilterController $filterController
	 */
	public function __construct(BaseOptions $options, array $items, FilterController $filterController)
    {
		$active = true;
		$this->title = $options->getTitle();
		$this->options = $options;
		$this->filterController = $filterController;
		$this->isResets = $options->isResets();

		$sortedItems = $this->sortFilterItems($items);
		$this->items = array_combine(
			array_map(static function ($item) {
                return Item::urlSafeTitle($item->getTitle());
            }, $sortedItems),
			array_map(function ($item) use (&$active) {
                if ($this->options instanceof CategoryOptions) {
                    FilterController::$allMediaIds += array_combine($item->getMediaIds(), array_fill(0, count($item->getMediaIds()), true));
                    //array_map(function ($id) { FilterController::$allMediaIds[$id] = true; }, $item->getMediaIds());
                }

                $item = Item::fromFilterItem($item, $this, null, $this->getSelectedFilters());
                if ($item->isActive()) {
                    $active = false;
                }

                return $item;
            },
            $sortedItems)
        );

		if ($options->isShowAllOption()) {
			$all = Item::allItem(
				$this,
				$active,
				FilterController::$allMediaIds
			);

			array_unshift($this->items, $all);
		}
	}

	/**
	 * @return BaseOptions
	 */
	public function getOptions()
    {
		return $this->options;
	}

	/**
	 * @param BaseOptions $options
	 * @return $this
	 */
	public function setOptions($options)
    {
		$this->options = $options;
		return $this;
	}

	/**
	 * @return Item[]
	 */
	public function getItems()
    {
		return $this->items;
	}

	/**
	 * @param Item $item
	 * @return $this
	 */
	public function addItem(Item $item)
    {
		$this->items[] = $item;
		return $this;
	}

	/**
	 * @param Item[] $items
	 * @return $this
	 */
	public function setItems($items)
    {
		$this->items = $this->sortFilterItems($items);
		return $this;
	}

	/**
	 * @return Data
	 */
	public function getSelectedFilters()
    {
		return $this->filterController->getSelectedFilters();
	}

	/**
	 * @param FilterItem[] $items
	 * @return array|FilterItem[]
	 */
	public function sortFilterItems(array $items)
    {
		if (is_callable($this->options->getSort())) {
			usort($items, $this->options->getSort());
		} elseif ($this->options->getSort() === BaseOptions::SORT_ALPHANUMERIC_ASC) {
			usort($items, static function ($a, $b) {
                return strcmp($a->getTitle(), $b->getTitle());
            });
		} elseif ($this->options->getSort() === BaseOptions::SORT_ALPHANUMERIC_DESC) {
			usort($items, static function ($a, $b) {
                return strcmp($b->getTitle(), $a->getTitle());
            });
		}

		return $items;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
    {
		return $this->title;
	}

	/**
	 * @param mixed $title
	 * @return $this
	 */
	public function setTitle($title)
    {
		$this->title = $title;
		return $this;
	}

	public function isResets(): bool
    {
		return $this->isResets;
	}
}
