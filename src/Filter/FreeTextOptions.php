<?php

namespace QBNK\FrontendComponents\Filter;

class FreeTextOptions extends BaseOptions
{
	public const FREETEXT_AND = 'and';
	public const FREETEXT_OR = 'or';

	public const WILDCARD_DONT_ADD = 0;
	public const WILDCARD_ADD_AFTER = 1;
	public const WILDCARD_ADD_BEFORE = 2;
	public const WILDCARD_ADD_BOTH = 3;

	/** @var string */
	protected $query;

	/** @var string */
	protected $operator = self::FREETEXT_AND;

	/** @var int */
	protected $wildCardMode;

	/**
	 * FreeTextOptions constructor.
	 * @param string $query
	 * @param string $operator
	 * @param string $mode
	 * @param int $wildCardMode If and how the wildcard char is appended between the input search terms
	 */
	public function __construct($query, $operator = self::FREETEXT_AND, $mode = self::FILTER_INTERSECT, $wildCardMode = self::WILDCARD_ADD_AFTER)
    {
		parent::__construct('Freetext', false, false, $mode);
		$this->showAllOption = false;
		$this->query = $query;
		$this->operator = $operator;
		$this->wildCardMode = $wildCardMode;
	}

	/**
	 * @return string
	 */
	public function getQuery()
    {
		return $this->query;
	}

	/**
	 * @param string $query
	 * @return $this
	 */
	public function setQuery($query)
    {
		$this->query = $query;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getOperator()
    {
		return $this->operator;
	}

	/**
	 * @param string $operator
	 * @return $this
	 */
	public function setOperator($operator)
    {
		$this->operator = $operator;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getWildCardMode(): int
	{
		return $this->wildCardMode;
	}

	/**
	 * @param int $wildCardMode
	 * @return $this
	 */
	public function setWildCardMode(int $wildCardMode): FreeTextOptions
	{
		if (!in_array($wildCardMode, [self::WILDCARD_DONT_ADD, self::WILDCARD_ADD_AFTER, self::WILDCARD_ADD_BEFORE, self::WILDCARD_ADD_BOTH], true)) {
			throw new \InvalidArgumentException('Invalid wild card mode for free text option');
		}
		$this->wildCardMode = $wildCardMode;
		return $this;
	}
}
