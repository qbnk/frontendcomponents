<?php

namespace QBNK\FrontendComponents\Filter;

class FolderOptions extends BaseOptions
{
	/** @var int */
	protected $parentId;

	/**
	 * FolderOptions constructor.
	 * @param string $title
	 * @param int $parentId
	 * @param bool $resets
	 * @param string $mode
	 * @param callable|string $sort
	 */
	public function __construct($title, $parentId, $resets = false, $mode = self::FILTER_INTERSECT, $sort = self::SORT_ALPHANUMERIC_ASC)
    {
		parent::__construct($title, $resets, false, $mode, $sort);
		$this->parentId = $parentId;
	}

	/**
	 * @return int
	 */
	public function getParentId()
    {
		return $this->parentId;
	}

	/**
	 * @param int $parentId
	 * @return $this
	 */
	public function setParentId($parentId)
    {
		$this->parentId = $parentId;
		return $this;
	}
}
