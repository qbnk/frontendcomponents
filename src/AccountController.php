<?php

namespace QBNK\FrontendComponents;

use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\QBankApi;

class AccountController
{
	const EXCEPTION_EMAIL_EXISTS = 1;

	public function getUniqueUsernameAndEmail(string $username, string $email, QBankApi $qbankApi)
	{
		$currentusers = [];
		$email = trim(strtolower($email));
		foreach ($qbankApi->accounts()->listUsers(true, new CachePolicy(false, 0)) as $user) {
			if (trim(strtolower($user->getEmail())) === $email) {
				throw new \Exception('Email already taken', self::EXCEPTION_EMAIL_EXISTS);
			}
			$currentusers[$user->getUserName()] = true;
		}

		if (array_key_exists($username, $currentusers)) {
			$i = 2;
			while (array_key_exists($username . $i, $currentusers)) {
				$i++;
			}
			$username .= $i;
		}

		return $username;
	}
}
