<?php

namespace QBNK\FrontendComponents;

use GuzzleHttp\Exception\RequestException;
use MailchimpTransactional\ApiClient;

abstract class MailController
{

	protected string $mandrillApiKey = 'extend me';

	/*
	* @param string $mandrillApiKey
	 */
	public function __construct(String $mandrillApiKey) {
		$this->mandrillApiKey = $mandrillApiKey;
	}

	protected function sendMail($subject, $html, $plain, $to = [], $from = 'QBank Robot', $tags = [])
	{
		$mailchimp = new ApiClient();
		$mailchimp->setApiKey($this->mandrillApiKey);

		try {
			return $mailchimp->messages->send([
				'subject' => $subject,
				'html' => (empty($html) ? $plain : $html),
				'text' => (empty($plain) ? $html : $plain),
				'from_email' => 'no-reply@qbank.se',
				'from_name' => $from,
				'to' => $to,
				'tags' => array_merge($tags)
			]);
		} catch (RequestException $e) {
			throw $e;
		}
	}

	protected function sendMailUsingTemplate($template, $templateVars = [], $to = [], $tags = [], ?string $subAccount = null)
	{
		$mailchimp = new ApiClient();
		$mailchimp->setApiKey($this->mandrillApiKey);

		try {
			return $mailchimp->messages->sendTemplate([
				'template_name' => $template,
				'template_content' => $templateVars,
				'message' => [
					'to' => $to,
					'tags' => array_merge(['media-portal-template'], $tags),
					'subaccount' => $subAccount,
					'global_merge_vars' => $templateVars,
				]
			]);
		} catch (RequestException $e) {
			throw $e;
		}
	}
}
