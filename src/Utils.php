<?php

declare(strict_types=1);

namespace QBNK\FrontendComponents;

use Jaybizzle\CrawlerDetect\CrawlerDetect;

class Utils
{
	/**
	 * Detects crawlers - either from a provided user agent or from global $_SERVER
	 * @param string|null $userAgent
	 * @return bool
	 */
	public static function isCrawler(?string $userAgent = null)
    {
		$crawlerDetect = new CrawlerDetect();
		return $crawlerDetect->isCrawler($userAgent);
	}

	/**
	 * Formats a filesize in bytes to a human-readable format.
	 * @param int $size The filesize in bytes.
	 * @param int $precision The precision to display in the result.
	 * Note: A precision of 0 will be used for sizes less then 1 kB.
	 * @param bool $truncateDecimals If true, trailing decimal zeroes will be printed, else they will not.
	 * @author Björn Hjortsten
	 * @return string A human-readable string representing the size.
	 */
	public static function formatSize(int $size, int $precision = 2, bool $truncateDecimals = true): string
    {
		$sizes = [' Bytes', ' kB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB'];
		if ($size === 0) {
			return('n/a');
		}

		$power = (int)floor(log($size, 1024));

		if ($power === 0) {
			$precision = 0;
		}

		if ($truncateDecimals === true) {
			$localeInfo = localeconv();
			return number_format(
                round($size / (1024 ** $power), $precision),
                $precision,
                $localeInfo['decimal_point'],
                $localeInfo['thousands_sep']
            ) . $sizes[$power];
		}

		return round($size / (1024 ** $power), $precision) . $sizes[$power];
	}

	/**
	 * * Helper method that checks if a string ($haystack) starts with another string ($needle)
	 *
	 * @param string $haystack
	 * @param string $needle
	 * @return bool
	 */
	public static function startsWith(string $haystack, string $needle): bool
    {
		return strpos($haystack, $needle) === 0;
	}

	/**
	 * Helper method that checks if a string ($haystack) ends with another string ($needle)
	 * @param string $haystack
	 * @param string $needle
	 * @return bool
	 */
	public static function endsWith(string $haystack, string $needle): bool
    {
		$length = strlen($needle);
		if ($length === 0) {
			return true;
		}

		return (substr($haystack, -$length) === $needle);
	}
}
