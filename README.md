Settings sample for the Account-manager
```php
$settings = [
        'account-manager' => [
			'qbank-manager-group-ids' => [20],
			'terms' => 'Lorem ipsum dolor...',
			'account-expire' => '+1 year',      // set false to disable this setting
			'apply-page-group-ids' => [1,2,3],
			'approve-page-group-ids' => [1,2,3,4],
			'custom-options' => [
				'Company' => [
					'array' => false,
					'mandatory' => true
				],
				'Country' => [
					'array' => true,
					'mandatory' => true,
					'multiple' => true,
					'options' => [
						'Sweden',
						'Norway',
						'Denmark'
					]
				]
			]
		]
    ]
```

### How to implement...
The following chapters should explain how certain components are implemented and used in front end projects.

### Download multiple fies
Since v3.0 the functionality for downloading multiple media has been updated and `DownloadController::downloadMultiple()` now requires different parameters.
It no longer accepts a template in the parameter, instead, the download templates are set via `DownloadController::setDownloadTemplate()`. This method accepts a classification (eg. `MimeType::CLASSIFICATION_IMAGE`) and a template id.
When getting the physical files, `DownloadController` uses `DownloadController::getDownloadTemplate()` for the medias classification. If you set a template to `0` or `null`, the original file will be fetched.

The media `DownloadController::downloadMultiple()` downloads are either passed as an array of MediaResponse or as an array of media ids (that must be numeric).

Example code:
```php
$media = [
    1337,
    999,
    123,
];
// or an array of MediaResponse, eg. from a QBank search

$downloadController = new DownloadController($this->qbankApi);
		
$imageTemplates = (int) $request->getQueryParam('imageTemplates');
$videoTemplate = (int) $request->getQueryParam('videoTemplate');

// If a specific classification has more than one template, call addDownloadTemplate multiple times
foreach($imageTemplates as $imageTemplate) {
    $downloadController->addDownloadTemplate(MimeType::CLASSIFICATION_IMAGE, $imageTemplate);
}
$downloadController->addDownloadTemplate(MimeType::CLASSIFICATION_VIDEO, $videoTemplate);

$downloadController->downloadMultiple($media, $deploymentSiteId, $sessionId, $zipFileName, $deploymentPath, $debug);
```

`DownloadController::downloadMultiple` throws a number of exceptions, so it can be a good idea to wrap in it a try-catch that catches `Throwable`, logs the error and produces a user friendly error message.

`DownloadController::setDownloadTemplate` currently accepts four (4) classifications, corresponding to:
`MimeType::CLASSIFICATION_IMAGE`, `MimeType::CLASSIFICATION_VIDEO`, `MimeType::CLASSIFICATION_AUDIO`, and `MimeType::CLASSIFICATION_DOCUMENT` which are the four configurable template types in QBank. For other classifications, the original file is always downloaded.

`DownloadController::downloadMultiple` does not support fetching multiple physical files from the same media, instead this sort of functionality must be implemented in that specific project if needs be.

####  Middleware\IPAutoLogin
To correctly implement the ``IPAutoLogin``, the settings array must contain an
array of allowed IPs, as well as a username reflecting a true user with the appropriate access:
```php
$settings[QB_SETTINGS][QB_FRONTEND][QB_IP_AUTO_LOGIN] = [
    'username' => 'apiuser',
    'firstname' => 'Api',
    'lastname' => 'User',
    'allowedIps' => [
        '123.45.67.89',
        '123.45.*.*'
    ]
];
```
An important note is that, since the ``IPAutoLogin`` creates a session depending on the users IP adress,
this must be done before ``RequireAuthenticatedUser``.  
This is because the ``RequireAuthenticatedUser`` middleware looks for an authenticated user (which ``IPAutoLogin`` creates).  
To make it execute before ``RequireAuthenticatedUser``, add it **after** - since Slim invokes middleware outside in.

Since v2.6, ``IPAutoLogin`` supports using wild cards in IP adresses. Use an asterisk (*) to denote wild card portions of the IP address. Note that the plugin currently only supports IPV4 addresses.  
```php
// Invoke IPAutoLogin AFTER RequireAuthenticated user
// as Slim invokes middleware from outside and in
$app->group('', function () use ($app) {
    $app->get('/someUrl', SomeClass::class . ':index');
})->add(RequireAuthenticatedUser::class)->add(IPAutoLogin::class);
````

#### Middleware\Translation
This middleware implements translation with the ``gettext()`` functions in PHP.
Somewhere in ``routes.php`` add ``$app->add(Translation::class);``, so that the middleware is registered.

Firstly, the middleware gets the ``HTTP_ACCEPT_LANGUAGE`` header from the users browser. Secondly it checks for a cookie.
If the cookie exists, this takes precedence over the ``HTTP_ACCEPT_LANGUAGE``. 

You must define both the cookie name to get user selected language, as well as the keys to the settings array for the ``gettext()`` configurating functions.
This configuration might be added to the ``default.php`` config.
```php
define('QB_FRONTEND_TRANSLATION','frontendtranslation');
$settings[QB_SETTINGS][QB_FRONTEND][QB_FRONTEND_TRANSLATION] = [
    'directory' => realpath(__DIR__ . '/../locales'),
    'domain' => 'messages',
];
```

You must define the cookie name, this might also be added to ``default.php``
```php
define('QB_FRONTEND_LANG_COOKIE','qbankfrontendlanguage');
```

Lastly, you must be able to parse all source files after calls to ``gettext()`` which should use the translation files.  
The easiest way is to add a application console to the frontendproject and use the commands ``php app/console.php gettext:generate`` and ``gettext:compile``.

As of 2020-02-04, a well working implementation of the application console can be found in frontends/peab-mediaportal.