# FrontendComponents
Contains shared Controllers for our Slim-based frontends.

###[3.0]
Improves the download controller for handling ZIP downloads.  
Please note that this version contains breaking changes for the DownloadController, when downloading multiple medias.

###[2.7]
Updates the QBank3 PHP API Wrapper to ~3.2

###[2.6.5]
Allows the filter controller to accept a custom cache policy.

###[2.6.4]
Adds a method for setting categories manually, which can be used to skip fetching the categories via API.

###[2.6.3]
 * Update to QBank3 PHP API Wrapper ~3.0

###[2.6.2]
 * composer.lock removed as to not lock any version

###[2.6.1]
 * Hotfix: An erroneous namespace is fixed

###[2.6]
 * IPAutoLogin now supports IPV4 adresses with wild cards

###[2.5]
* Added: SelectionAwareRoutes middleware

####[2.4.1]
* Hotfix: Add correct namespace for Translation middleware

####[2.4]
* Added: Translation middleware

####[2.3]
* Added: IPAutoLogin middleware, requires an array setting of allowed IPs, as well as a username reflecting a true user with the appropriate access
* Added: Sample settings file
* Declared Slim 3.* requirement

####[2.2.16]
* Added: AccountManager and sample config (README) for handling request and management of QBank accounts

####[2.2.15]
* Bugfix: Added safeguard for build link of hierarchical filter items where a node might fail.
* Added: RequireAuthenticatedUser middle ware now stores path and query in the query parameter "next" when redirecting to login page.

####[2.2.13] 
* Changed: Helper class Utils is now declared strict
* Added: static method Utils::startsWith(string $haystack, string $needle): bool
* Added: static method Utils::endsWith(string $haystack, string $needle): bool 

####[2.2.11] 
* Changed: QBank API wrapper uses strict redirects<br>
Before: POST => {redirect} => GET<br>
Now: POST => {redirect} => POST

####[2.2.10] 
* Bugfix: Don't merge empty leaf values of hierarchical properties when calculating links 

####[2.2.9] 
* Bugfix: Use union operator instead when merging path nodes for hierarchical properties

####[2.0.3]
* Changed: Updated the QBank API wrapper - now supports folder search
* Added: Use constants for property criteria operators

####Earlier
* **2017-04-20** Initial commit, currently includes DownloadController and FilterController.
